<?php
/* 
Plugin Name: LocalSphere Custom Dev
Description: Creates an activation code system that provides login credentials for the Greens Card app. 
Author: LocalSphere
Author URI: http://localsphere.com
Version: 1.0
*/

// Create tables on plugin activation

register_activation_hook( __FILE__, 'ls_install' );

function ls_install() {

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	global $wpdb;
	$charset_collate = $wpdb->get_charset_collate();

	// Create cardusers table

	$card_table_name = $wpdb->prefix . 'cardusers';

	$card_sql = "CREATE TABLE $card_table_name (
  			  cardnumber VARCHAR(19),
  			  activationcode INT,
			  username VARCHAR(8),
			  password VARCHAR(8),
			  email VARCHAR(30)
			) $charset_collate;";

	dbDelta( $card_sql );

	// Create virtualusers table

	$virtual_table_name = $wpdb->prefix . 'virtualusers';

	$virtual_sql = "CREATE TABLE $virtual_table_name (
			  username VARCHAR(8),
			  password VARCHAR(8),
			  email VARCHAR(30)
			) $charset_collate;";

	dbDelta( $virtual_sql );
}

require_once('includes/db.php');
require_once('includes/email.php');
require_once('includes/forms.php');
require_once('includes/shortcodes.php');
require_once('includes/woocommerce.php');

?>