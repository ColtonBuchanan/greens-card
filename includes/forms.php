<?php

// Activation form validation function called by Caldera Forms

add_filter( 'ls_validate_activation_form', 'ls_validate_activation_form' );

function ls_validate_activation_form($data) {

	$result = ls_check_activation( $data['activation_code'], $data['pin'] );

	if ( $result == false ) {

		return array(
            'type' => 'error',
            'note' => 'Wrong activation code'
        );

	} else {
		ls_email_login( $data['full_name'], $data['email_address'], $result[0]->username, $result[0]->password, true );
	}
}


// Recover Login form validation function called by Caldera Forms

add_filter( 'ls_validate_recover_login_form', 'ls_validate_recover_login_form' );

function ls_validate_recover_login_form($data) {

	$result = ls_get_login_by_email( $data['email_address'] );

	if ( $result == false ) {

		return array(
            'type' => 'error',
            'note' => 'Email address not found'
        );

	} else {
		ls_email_login( '', $data['email_address'], $result['username'], $result['password'], false );
	}
}

?>