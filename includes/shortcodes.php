<?php

// Creates a shortcode for displaying a users login after form submission

function ls_display_login_after_form() {

	$result = ls_check_activation( $_GET['activation_code'], $_GET['pin'] );

	$html = 'Username: ' . $result[0]->username . ' Password: ' . $result[0]->password;

	if ( $result ) {
		ls_add_email_to_login( $result[0]->username, $_GET['email_address'], false );
	}

	return $html;
}

add_shortcode( 'show_login', 'ls_display_login_after_form' );

?>