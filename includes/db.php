<?php

// Check if there's a matching card number and activation code in the database

function ls_check_activation( $cardnumber, $activationcode ) {

	global $wpdb;

	$result = $wpdb->get_results( $wpdb->prepare(
		"SELECT * FROM wp_cardusers WHERE cardnumber = %s AND activationcode = %s AND email IS NULL",
		$cardnumber, $activationcode
	));

	if ( ! empty($result) ) {
		return $result;
	} else {
		return false;
	}
}


// Get login by email

function ls_get_login_by_email( $email ) {

	global $wpdb;

	$result = $wpdb->get_row( $wpdb->prepare(
		"SELECT username, password, email FROM wp_cardusers WHERE email = %s UNION SELECT username, password, email FROM wp_virtualusers WHERE email = %s LIMIT 1",
		$email, $email
	), ARRAY_A);

	return $result;

}


// Retrieve a new login

function ls_get_new_login( $virtual = true, $email = null ) {

	global $wpdb;

	if ( $virtual ) {
		$result = $wpdb->get_row( "SELECT * FROM wp_virtualusers WHERE email IS NULL LIMIT 1", ARRAY_A );

	} else {
		$result = $wpdb->get_row( "SELECT * FROM wp_cardusers WHERE email IS NULL LIMIT 1", ARRAY_A );
	}

	if ( $email ) {
		ls_add_email_to_login( $result['username'], $email, $virtual );
	}

	return $result;

}


// Add an email to a login

function ls_add_email_to_login( $username, $email, $virtual ) {

	global $wpdb;

	if ( $virtual ) {
		$table = 'wp_virtualusers';

	} else {
		$table = 'wp_cardusers';
	}
	
	$result = $wpdb->update( $table, array( 'email' => $email ), array( 'username' => $username ) );

}


// Remove a login from the database

function ls_delete_login( $username, $virtual ) {

	global $wpdb;

	if ( $virtual ) {
		$result = $wpdb->delete( 'wp_virtualusers', array( 'username' => $username ) );

	} else {
		$result = $wpdb->delete( 'wp_cardusers', array( 'username' => $username ) );
	}

	return $result;
}

?>