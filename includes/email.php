<?php

// Template for showing login in emails

function ls_display_login_in_email( $username, $password, $header, $name ) {

	$table = '';

	if ( $header == true ) {
		$table .= '<table>
						<tbody>
							<tr>
								<th style="background-color: #111; padding: 24px;">
									<img src="http://greenscard.com/wp-content/uploads/2016/02/logo-2016-whitecard.png" />
								</th>

								<tr>
									<p style="font-size: 15px;">Hey ' . $name . ', </p>
									<p style="font-size: 15px;">Thank you for activating your Greens Card account. You can now use the login credentials below to access the Greens Card app. We\'ll see you there!</p>
							</tr>
						</tbody>
					</table>';
	}

	$table .= '<table>
					<tbody>
						<tr>
							<th style="width: 300px; border: 1px solid #DDD; font-size: 15px; text-align: center;">Username</th>
							<th style="width: 300px; border: 1px solid #DDD; font-size: 15px; text-align: center;">Password</th>
						</tr>
						<tr>
							<td style="width: 300px; border: 1px solid #DDD; font-size: 15px; text-align: center;">' . $username . '</td>
							<td style="width: 300px; border: 1px solid #DDD; font-size: 15px; text-align: center;">' . $password . '</td>
						</tr>
					</tbody>
				</table>';

	$table .= '<table style="margin-top: 24px;">
					<tbody>
						<tr>
							<td>
								<img style="max-width: 150px;" src="http://greenscard.com/wp-content/plugins/ls-custom-dev/assets/apple-store.png"/>
							</td>
							<td>
								<img style="max-width: 150px;" src="http://greenscard.com/wp-content/plugins/ls-custom-dev/assets/google-play-store.png"/>
							</td>
						</tr>
					</tbody>
				</table>';

	return $table;
}


// Email users their login info

function ls_email_login( $name, $email, $username, $password, $header = false ) {

	$subject  = 'Your login credentials for Greens Card';
	$headers  = 'From: Greens Card <noreply@greenscard.com>' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

	$message = ls_display_login_in_email( $username, $password, $header, $name );

	wp_mail( $email, $subject, $message, $headers );
}


// Email login credentials on Order Complete

add_action( 'woocommerce_order_status_completed', 'ls_order_complete' );

function ls_order_complete( $order_id ) {

	$order = wc_get_order( $order_id );

	$result = ls_get_new_login( true, $order->billing_email );

	ls_email_login( $order->billing_first_name, $order->billing_email, $result['username'], $result['password'], true );

}

?>