<?php
/* 
* This file is for changing WooCommerce behaviour
*/

// Empty cart before adding a product to it. This allows only 1 product to be purchased at a time.

add_filter( 'woocommerce_add_cart_item_data', 'ls_empty_cart_before_adding_to_it' );
 
function ls_empty_cart_before_adding_to_it( $cart_item_data ) {
	global $woocommerce;
	$woocommerce->cart->empty_cart();
	 
	return $cart_item_data;
}